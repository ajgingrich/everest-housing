const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const Dotenv = require('dotenv-webpack');
const CompressionPlugin = require('compression-webpack-plugin');

module.exports = {
  mode: 'production',
  entry: ['./src/index.jsx'],
  output: {
    path: path.join(__dirname, 'src/dist'),
    filename: 'bundle.js',
  },
  resolve: {
    alias: {
      'containers': path.join(__dirname, 'src/containers'), // eslint-disable-line quote-props
      'components': path.join(__dirname, 'src/components'), // eslint-disable-line quote-props
      'actions': path.join(__dirname, 'src/actions'), // eslint-disable-line quote-props
      'store': path.join(__dirname, 'src/store'), // eslint-disable-line quote-props
    },
    extensions: ['jsx', '.js', '.json'],
    modules: [path.join(__dirname, 'src/'), 'node_modules'],
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        resolve: {
          extensions: ['.js', '.jsx'],
        },
        use: ['babel-loader'],
        exclude: /node_modules/,
      },
      {
        test: /\.s?css$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          'postcss-loader',
          'sass-loader',
        ],
      },
      { test: /\.(jpg|png|woff|woff2|eot|ttf|svg)$/, use: 'url-loader?limit=1000000' },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: 'src/index.template.ejs',
      filename: '../index.html',
    }),
    new MiniCssExtractPlugin({
      filename: 'style.bundle.css',
    }),
    new Dotenv(),
    new CompressionPlugin({
      filename: '[path].gz[query]',
      algorithm: 'gzip',
      test: /\.js$|\.css$/,
      minRatio: 0.8,
    }),
  ],
};
