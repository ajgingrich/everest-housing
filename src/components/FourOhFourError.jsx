import React from 'react';
import PropTypes from 'prop-types';

const FourOhFourError = ({ location }) => (
  <div>
    <h3>
      No match for
      <code>{location && location.pathname}</code>
    </h3>
  </div>
);

FourOhFourError.propTypes = {
  location: PropTypes.string.isRequired,
};

export default FourOhFourError;
