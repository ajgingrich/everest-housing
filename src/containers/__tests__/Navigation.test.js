import React from 'react'
import { Navigation } from 'containers/Navigation/Navigation'
import ShallowRenderer from 'react-test-renderer/shallow'

const renderer = new ShallowRenderer()

it('Navigation renders correctly', () => {
  const tree = renderer
    .render(
      <Navigation />
    );
  expect(tree).toMatchSnapshot();
});
