import request from 'supertest'
import app from '../app'

describe('test user controller APIs', () => {
  test('It should response the GET method', () => {
      return request(app).get('/').expect(200);
  });
})
