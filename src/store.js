import { createStore, applyMiddleware, compose } from 'redux';
import { responsiveStoreEnhancer } from 'redux-responsive';
import thunk from 'redux-thunk';
import reducer from './reducers';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose; // eslint-disable-line no-underscore-dangle

export default function configureStore() {
  return createStore(
    reducer,
    composeEnhancers(
      responsiveStoreEnhancer,
      applyMiddleware(thunk),
    ),
  );
}
