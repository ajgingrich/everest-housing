import React from 'react'
import Footer from 'containers/Footer.jsx'
import ShallowRenderer from 'react-test-renderer/shallow'

const renderer = new ShallowRenderer()

it('Footer renders correctly', () => {
  const tree = renderer
    .render(
      <Footer />
    );
  expect(tree).toMatchSnapshot();
});
