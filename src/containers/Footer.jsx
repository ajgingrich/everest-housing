import React from 'react';

const Footer = () => (
  <div className="footer">
    <div className="container">
      <span>
        <a href="https://andrewgingrich.com/" rel="noopener noreferrer" target="_blank">© Andrew Gingrich 2019</a>
      </span>
    </div>
  </div>
);

export default Footer;
