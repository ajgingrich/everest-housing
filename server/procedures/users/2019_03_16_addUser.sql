CREATE PROCEDURE everest.`addUser`
(p_username varchar(32), p_password varchar(128),p_userrole int, p_firstname varchar(32), p_lastname varchar(32), p_twitter varchar(32))
BEGIN
	INSERT INTO everest.user
	(username, password, user_role, first_name, last_name, twitter)
	VALUES(p_username, p_password, p_userrole, p_firstname, p_lastname, p_twitter);
END;
