export const ERROR = 'error';
export const LOADING = 'loading';

export const defaultBreakpoints = {
  extraSmall: 480,
  small: 768,
  medium: 991,
  large: 1200,
};
