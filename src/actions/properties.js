// import { keyProperty } from 'helpers/keys';
import {
  PROPERTIES_ADD,
} from 'constants/actionTypes';
import {
  API_PROPERTIES_ADD,
} from 'constants/api';

import { peformPost } from './actionCreator';

export function propertyAdd(query) {
  return peformPost({
    type: PROPERTIES_ADD,
    location: API_PROPERTIES_ADD,
    body: query,
  });
}
