import { user } from '../users'
import * as types from 'constants/actionTypes'
import { ACTION_SUCCESS, ACTION_PENDING, ACTION_FAILURE } from 'constants/actionTypes';
import { LOADING } from 'constants/constants';

describe('users reducer', () => {
  const mockUser = {
    email: 'admin-mock@yahoo.com',
    role: 1,
    firstName: 'john',
    lastName: 'smith',
  };

  it('should return the initial state', () => {
    expect(user(undefined, {})).toEqual({})
  })

  it('should handle USER_SIGN_UP', () => {
    expect(
      user({}, {
        type: `${types.USER_SIGN_UP}.${ACTION_PENDING}`,
      })
    ).toBe(LOADING)

    expect(
      user({}, {
        type: `${types.USER_SIGN_UP}.${ACTION_SUCCESS}`,
        payload: mockUser,
      })
    ).toEqual(mockUser)
  })

  it('should handle USER_LOGIN', () => {
    expect(
      user({}, {
        type: `${types.USER_LOGIN}.${ACTION_PENDING}`,
      })
    ).toBe(LOADING)

    expect(
      user({}, {
        type: `${types.USER_LOGIN}.${ACTION_SUCCESS}`,
        payload: mockUser,
      })
    ).toEqual(mockUser)
  })

  it('should handle USER_FETCH', () => {
    expect(
      user({}, {
        type: `${types.USER_FETCH}.${ACTION_PENDING}`,
      })
    ).toBe(LOADING)

    expect(
      user({}, {
        type: `${types.USER_FETCH}.${ACTION_SUCCESS}`,
        payload: mockUser,
      })
    ).toEqual(mockUser)
  })

  it('should handle USER_UPDATE', () => {
    expect(
      user({}, {
        type: `${types.USER_UPDATE}.${ACTION_PENDING}`,
      })
    ).toBe(LOADING)

    expect(
      user({}, {
        type: `${types.USER_UPDATE}.${ACTION_SUCCESS}`,
        payload: mockUser,
      })
    ).toEqual(mockUser)
  })

  it('should handle USER_LOGOUT', () => {
    expect(
      user({}, {
        type: `${types.USER_LOGOUT}.${ACTION_PENDING}`,
      })
    ).toBe(LOADING)

    expect(
      user({}, {
        type: `${types.USER_LOGOUT}.${ACTION_SUCCESS}`,
        payload: {},
      })
    ).toEqual({})
  })

  it('should handle USER_CLEAR', () => {
    expect(
      user({}, {
        type: types.USER_CLEAR,
        payload: {},
      })
    ).toEqual({})
  })
})
