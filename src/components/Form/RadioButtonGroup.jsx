import React from 'react';
import PropTypes from 'prop-types';

const RadioButtonGroup = ({
  classname,
  children,
}) => (
  <div className={classname}>
    <fieldset>
      {children}
    </fieldset>
  </div>
);

RadioButtonGroup.defaultProps = {
  classname: null,
};

RadioButtonGroup.propTypes = {
  classname: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]).isRequired,
};

export default RadioButtonGroup;
