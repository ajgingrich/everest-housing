import jwt from 'jsonwebtoken';

import {
  API_USER_LOGIN,
  API_USER_FETCH,
  API_USER_ADD,
  API_USER_LOGOUT,
} from '../../src/constants/api';

require('dotenv').config();

export const generateJWTToken = email => jwt.sign({ email }, process.env.JWT_SECRET, { expiresIn: '4h' });

export const verifyJWTToken = (req, res, next) => { // eslint-disable-line consistent-return
  const allowedUrls = [
    API_USER_FETCH,
    API_USER_LOGIN,
    API_USER_ADD,
    API_USER_LOGOUT,
    '/',
    '/dist/style.bundle.css',
    '/dist/bundle.js',
  ];

  if (allowedUrls.indexOf(req.originalUrl) !== -1) {
    return next();
  }

  try {
    const authorization = req.headers.authorization && req.headers.authorization.split(' ');
    const token = authorization[0] === 'Bearer' && authorization[1];
    const decoded = jwt.verify(token, process.env.JWT_SECRET); // eslint-disable-line no-unused-vars

    next();
  } catch (err) {
    res.status(401).send('Unauthorized user');
  }
};
