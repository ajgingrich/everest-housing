const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const Dotenv = require('dotenv-webpack');

module.exports = {
  mode: 'development',
  entry: [
    'react-hot-loader/patch',
    'webpack-hot-middleware/client',
    './src/index.jsx',
  ],
  devtool: 'inline-source-map',
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new HtmlWebpackPlugin({
      template: 'src/index.template.ejs',
      filename: '../index.html',
    }),
    new MiniCssExtractPlugin({
      filename: 'style.bundle.css',
    }),
    new Dotenv(),
  ],
  resolve: {
    alias: {
      'containers': path.join(__dirname, 'src/containers'), // eslint-disable-line quote-props
      'components': path.join(__dirname, 'src/components'), // eslint-disable-line quote-props
      'actions': path.join(__dirname, 'src/actions'), // eslint-disable-line quote-props
      'store': path.join(__dirname, 'src/store'), // eslint-disable-line quote-props
    },
    extensions: ['jsx', '.js', '.json'],
    modules: [path.join(__dirname, 'src/'), 'node_modules'],
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        resolve: {
          extensions: ['.js', '.jsx'],
        },
        use: ['babel-loader'],
        exclude: /node_modules/,
      },
      {
        test: /\.s?css$/,
        use: [
          'style-loader',
          'css-loader',
          'postcss-loader',
          'sass-loader',
        ],
      },
      { test: /\.(jpg|png|woff|woff2|eot|ttf|svg)$/, use: 'url-loader?limit=1000000' },
    ],
  },
  output: {
    path: path.join(__dirname, 'src/dist'),
    filename: 'bundle.js',
    publicPath: '/',
  },
};
