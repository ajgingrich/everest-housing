import React from 'react';
import PropTypes from 'prop-types';

const RadioButton = ({
  field: { name, value, onChange },
  id,
}) => (
  <input
    name={name}
    id={id}
    type="radio"
    value={id}
    checked={id === value}
    onChange={onChange}
  />
);

RadioButton.propTypes = {
  field: PropTypes.object.isRequired,
  id: PropTypes.string.isRequired,
};

export default RadioButton;
