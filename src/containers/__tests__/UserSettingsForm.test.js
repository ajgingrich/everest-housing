import React from 'react'
import UserSettingsForm from 'containers/UserSettings/UserSettingsForm'
import ShallowRenderer from 'react-test-renderer/shallow'

const renderer = new ShallowRenderer()

it('UserSettingsForm renders correctly', () => {
  const tree = renderer
    .render(
      <UserSettingsForm />
    );
  expect(tree).toMatchSnapshot();
});
