import express from 'express';
import path from 'path';
import fs from 'fs';
import favicon from 'serve-favicon';
import webpack from 'webpack';
import bodyParser from 'body-parser';
import session from 'express-session';

import { verifyJWTToken } from './auth/auth';

const MySQLStore = require('express-mysql-session')(session);
const routes = require('./routes/index');
const config = require('../webpack.config');
require('dotenv').config();

const mySQLSessionOptions = {
  host: process.env.MYSQL_HOST,
  user: process.env.MYSQL_USER,
  password: process.env.MYSQL_PASSWORD,
  database: process.env.MYSQL_DATABASE,
  expiration: 86400000,
  createDatabaseTable: true,
};

const sessionStore = new MySQLStore(mySQLSessionOptions);
const compiler = webpack(config);
const app = express();

const IS_DEVELOPMENT = app.get('env') === 'development';
const sessionConfig = {
  resave: false,
  saveUninitialized: false,
  secret: process.env.SESSION_SECRET,
  store: sessionStore,
};

app.use(favicon(path.join(__dirname, '../src', 'img', 'favicon.ico')));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(session(sessionConfig));

if (IS_DEVELOPMENT) {
  app.use(require('webpack-dev-middleware')(compiler, {
    noInfo: true,
    publicPath: config.output.publicPath,
  }));

  app.use(require('webpack-hot-middleware')(compiler));

  app.get('/', (req, res, next) => {
    compiler.outputFileSystem.readFile(
      path.join(__dirname, '../src/index.html'),
      (err, result) => { // eslint-disable-line consistent-return
        if (err) {
          return next(err);
        }
        res.set('content-type', 'text/html');
        res.send(result);
        res.end();
      },
    );
  });
} else {
  const serveGzip = contentType => (req, res, next) => {
    req.url += '.gz';
    res.set('Content-Encoding', 'gzip');
    res.set('Content-Type', contentType);

    next();
  };

  app.get('*.js', serveGzip('text/javascript'));
  app.get('*.css', serveGzip('text/css'));
}

// view engine setup
app.set('view engine', 'html');
app.engine('html', (pathEngine, options, callback) => {
  fs.readFile(pathEngine, 'utf-8', callback);
});

app.all('*', verifyJWTToken);
app.use(express.static(path.join(__dirname, '../src')));
app.use('/', routes);

// catch error and send to client
app.use((err, req, res, next) => { // eslint-disable-line no-unused-vars
  res.status(err.status || 500);
  res.send(err);
});

export default app;
