import React from 'react';
import {
  HashRouter as Router,
  Route,
  Switch,
} from 'react-router-dom';
import {
  Home,
  Navigation,
  UserSettings,
  Properties,
  Footer,
} from 'containers';
import { FourOhFourError } from 'components';

const Routes = () => (
  <Router>
    <div>
      <Navigation />
      <div className="container">
        <div className="mainWrapper">
          <div className="main">
            <Switch>
              <Route exact path="/" component={Home} />
              <Route exact path="/user/settings" component={UserSettings} />
              <Route exact path="/properties" component={Properties} />
              <Route component={FourOhFourError} />
            </Switch>
          </div>
        </div>
        <Footer />
      </div>
    </div>
  </Router>
);

export default Routes;
