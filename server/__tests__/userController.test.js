import request from 'supertest'
import chai from 'chai'

import app from '../app'

import { API_USER_LOGIN, API_USER_FETCH, API_USER_ADD, API_USER_LOGOUT, API_USER_UPDATE, API_USER_DELETE } from '../../src/constants/api';

import { generateJWTToken } from '../auth/auth';

describe('test user controller APIs', async () => {
  const expect = chai.expect;
  const mockUser = {
    email: 'admin-mock@yahoo.com',
    password: 'test123',
    role: 1,
    firstName: 'john',
    lastName: 'smith',
  };

  describe('#POST /user/add', () => {
    it('should add a user', async done => {
      const res = await request(app).post(API_USER_ADD).send(mockUser);

      expect(res.statusCode).to.equal(200);
      expect(res.body).to.be.an('object');
      expect(res.body.email).to.equal(mockUser.email);
      done();
    });
  })

  describe('#POST /user/add', () => {
    it('should return error on duplicate user', async done => {
      const res = await request(app).post(API_USER_ADD).send(mockUser);

      expect(res.statusCode).to.equal(200);
      expect(res.body).to.be.an('object');
      expect(res.body.error).to.equal('Email address already in use');
      done();
    });
  })

  describe('#POST /user/login', () => {
    it('should login user', async done => {
      const res = await request(app).post(API_USER_LOGIN).send({ email: mockUser.email, password: mockUser.password });

      expect(res.statusCode).to.equal(200);
      expect(res.body).to.be.an('object');
      expect(res.body.email).to.equal(mockUser.email);
      done();
    });
  })

  describe('#POST /user/login', () => {
    it('should return error with invalid password', async done => {
      const res = await request(app).post(API_USER_LOGIN).send({ email: mockUser.email, password: 'notCorrect' });
      expect(res.statusCode).to.equal(200);
      expect(res.body.error).to.equal('Invalid email or password');
      done();
    });
  })

  describe('#POST /user/fetch', () => {
    it('should fetch user session', async done => {
      const res = await request(app).post(API_USER_FETCH);

      expect(res.statusCode).to.equal(200);
      expect(res.body).to.be.an('object');
      expect(res.body).to.eql({});
      done();
    });
  })

  describe('#PUT /user/update', () => {
    it('should update user', async done => {
      const userResult = await request(app).post(API_USER_LOGIN).send({ email: mockUser.email, password: mockUser.password });
      const { id: userId, lastName: userLastName, email: userEmail } = userResult.body
      const token = generateJWTToken(userEmail);
      const res = await request(app).put(API_USER_UPDATE).set('Authorization', `Bearer ${token}`).send({
        id: userId,
        role: 2,
        firstName: 'update',
        lastName: mockUser.lastNamee,
        twitter: 'tweety',
      });

      expect(res.statusCode).to.equal(200);
      expect(res.body).to.be.an('object');
      expect(res.body.role).to.equal(2);
      expect(res.body.firstName).to.equal('update');
      expect(res.body.twitter).to.equal('tweety');
      done()
    });
  })

  describe('#POST /user/logout', () => {
    it('should logout user', async done => {
      const res = await request(app).post(API_USER_LOGOUT).send();

      expect(res.statusCode).to.equal(200);
      expect(res.body).to.eql({});
      done();
    });
  })

  describe('#DELETE /user/delete', () => {
    it('should delete', async done => {
      const token = generateJWTToken(mockUser.email);
      const res = await request(app).delete(API_USER_DELETE).set('Authorization', `Bearer ${token}`).send({ email: mockUser.email });

      expect(res.statusCode).to.equal(200);
      expect(res.body).to.eql({ email: mockUser.email });
      done();
    });
  })
})
