import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { userLogin, userLogout, userFetch, userClear } from 'actions/users';
import { isValidUser, userLoginError, isUserLoading } from 'helpers/users';

import Login from './Login';

const mapStateToProps = ({ user }) => ({
  user,
  hasUser: isValidUser(user),
  loginError: userLoginError(user),
  loading: isUserLoading(user),
});

const mapDispatchToProps = dispatch => bindActionCreators({
  login: userLogin,
  logout: userLogout,
  fetchUser: userFetch,
  clear: userClear,
}, dispatch);

export class Navigation extends Component {
  constructor(props) {
    super(props);

    this.state = { isLoginOpen: false };
  }

  componentDidMount() {
    const { hasUser, fetchUser } = this.props;

    if (!hasUser) {
      fetchUser();
    }
  }

  handleToggleLogin = () => {
    const { isLoginOpen } = this.state;

    this.setState({ isLoginOpen: !isLoginOpen });
  }

  handleCloseLogin = () => {
    this.setState({ isLoginOpen: false });
  }

  handleLogout = () => {
    const { logout } = this.props;

    logout();
  }

  handleUserClear = () => {
    const { clear } = this.props;

    clear();
  }

  handleSignIn = (params) => {
    const { login } = this.props;

    login(params);
  }

  render() {
    const { loading, user, loginError, hasUser } = this.props;
    const { isLoginOpen } = this.state;
    const loginText = hasUser ? user.email : 'Login';
    const loginCaret = isLoginOpen ? <i className="fas fa-caret-up fa-sm" /> : <i className="fas fa-caret-down fa-sm" />;

    return (
      <div className="navigation">
        <div className="container">
          <ul>
            <li className="navInitials">
              <NavLink to="/">
                <span className="tooltip-bottom" data-tooltip="Home">
                  <i className="fas fa-mountain fa-sm" />
                  HOUSING
                </span>
              </NavLink>
            </li>
            <li>
              <span onClick={this.handleToggleLogin}>
                {loading ? 'loading' : loginText} {loginCaret}
              </span>
              {isLoginOpen && (
                <div className="login">
                  {hasUser && <div className="logout" onClick={this.handleLogout}>Logout</div>}
                  {!hasUser && (
                    <Login
                      onBlur={this.handleCloseLogin}
                      onCloseLogin={this.handleCloseLogin}
                      user={user}
                      onLogin={this.handleSignIn}
                      loginError={loginError}
                      onUserClear={this.handleUserClear}
                    />
                  )}
                </div>
              )}
            </li>
            {hasUser && user.role === 2 && (
              <li>
                <NavLink to="/properties">
                  <span className="tooltip-bottom" data-tooltip="Add Property">
                    <i className="fas fa-home fa-lg" />
                  </span>
                </NavLink>
              </li>
            )}
            { hasUser && user.role === 3 && (
              <li>
                <NavLink to="/admin">
                  <span className="tooltip-bottom" data-tooltip="Admin Page">
                    <i className="fas fa-tools fa-lg" />
                  </span>
                </NavLink>
              </li>
            )}
            <li>
              <NavLink to="/search">
                <span className="tooltip-bottom" data-tooltip="Search">
                  <i className="fas fa-search fa-lg" />
                </span>
              </NavLink>
            </li>
            <li>
              <NavLink to="/user/settings">
                <span className="tooltip-bottom" data-tooltip="Settings">
                  <i className="fas fa-user-cog fa-lg" />
                </span>
              </NavLink>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}

Navigation.defaultProps = {
  user: {},
  loginError: '',
  hasUser: false,
  loading: false,
};

Navigation.propTypes = {
  hasUser: PropTypes.bool,
  clear: PropTypes.func.isRequired,
  fetchUser: PropTypes.func.isRequired,
  login: PropTypes.func.isRequired,
  loading: PropTypes.bool,
  loginError: PropTypes.string,
  logout: PropTypes.func.isRequired,
  user: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
};

export default connect(mapStateToProps, mapDispatchToProps)(Navigation);
