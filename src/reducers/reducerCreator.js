import { ERROR, LOADING } from 'constants/constants';
import { ACTION_SUCCESS, ACTION_PENDING, ACTION_FAILURE } from 'constants/actionTypes';

export function reducerCreator({ state, action, type, onSuccess }) {
  switch (action.type) {
    case `${type}.${ACTION_PENDING}`:
      return LOADING;
    case `${type}.${ACTION_SUCCESS}`:
      return action.key ? onSuccess(action.payload) : action.payload;
    case `${type}.${ACTION_FAILURE}`:
      return ERROR;
    default:
      return state;
  }
}
