import React from 'react';
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from 'react-google-maps';

const Map = withScriptjs(withGoogleMap(({ isMarkerShown, lat, lng, defaultZoom, onChangeMarker }) => (
  <GoogleMap
    defaultZoom={defaultZoom}
    defaultCenter={{ lat, lng }}
    onClick={onChangeMarker}
  >
    {isMarkerShown && <Marker position={{ lat, lng }} />}
  </GoogleMap>
)));

export default Map;
