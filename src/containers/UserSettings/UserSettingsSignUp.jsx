import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { userLoginError } from 'helpers/users';
import { userSignUp } from 'actions/users';

import UserSettingsForm from './UserSettingsForm';

const INITIAL_VALUES = {
  email: '',
  password: '',
  passwordConfirm: '',
  radioGroup: 'user',
  firstName: '',
  lastName: '',
  twitter: '',
};

const mapStateToProps = ({ user }) => ({
  loginError: userLoginError(user),
});

const mapDispatchToProps = dispatch => bindActionCreators({
  signUp: userSignUp,
}, dispatch);

const validateErrors = (values) => {
  // add more validation here..
  const errors = {};

  if (!values.email) {
    errors.email = 'Required';
  } else if (
    !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
  ) {
    errors.email = 'Invalid email address';
  }

  return errors;
};

export class UserSettingsSignUp extends Component {
  handleSignUp = (values, setSubmitting) => {
    const { signUp } = this.props;
    const query = {
      ...values,
      role: values.radioGroup === 'user' ? 1 : 2,
    };

    signUp(query);
    setSubmitting(false);
  }

  render() {
    const { loginError } = this.props;

    return (
      <Fragment>
        <h3>Sign Up</h3>
        <UserSettingsForm
          onSubmit={this.handleSignUp}
          initialValues={INITIAL_VALUES}
          loginError={loginError}
          hasUser={false}
          onValidateErrors={validateErrors}
        />
      </Fragment>
    );
  }
}

UserSettingsSignUp.defaultProps = {
  loginError: '',
};

UserSettingsSignUp.propTypes = {
  loginError: PropTypes.string,
  signUp: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(UserSettingsSignUp);
