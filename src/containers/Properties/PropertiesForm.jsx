import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import {
  Formik,
  Form,
  Field,
  ErrorMessage,
} from 'formik';

import { RadioButton, RadioButtonGroup } from 'components/Form';
import { Map } from 'components';

const googleApiKey = process.env.GOOGLE_MAPS_API_KEY;
const GOOGLE_MAP_URL = `https://maps.googleapis.com/maps/api/js?v=3.35&key=${googleApiKey}`;

const UserSettingsForm = ({
  isEditingForm,
  initialValues,
  onValidateErrors,
  onSubmit,
  onChangeMarker,
  lat,
  lng,
}) => (
  <Fragment>
    <Formik
      initialValues={initialValues}
      validate={values => onValidateErrors(values)}
      onSubmit={(values, { setSubmitting }) => onSubmit(values, setSubmitting)}
    >
      {({ isSubmitting, values, errors }) => (
        <Form className="propertiesForm">
          <div className="propertiesFormRow">
            <span>Rent Per Month ($)</span> <Field type="number" name="rentPerMonth" />
            <ErrorMessage className="formError" name="rentPerMonth" component="div" />
          </div>
          <h4>Description</h4>
          <div className="propertiesFormRow">
            <span>Physical Description</span> <Field name="physicalDescription" placeholder="Enter a description" />
            <ErrorMessage className="formError" name="physicalDescription" component="div" />
          </div>
          <div className="propertiesFormRow">
            Number of Rooms
            <Field component="select" name="numberRooms">
              {(
                new Array(15).fill({ value: 1, label: 1 }).map((_, i) => (
                  <option key={`numberRooms-form-${i + 1}`} value={i + 1}>{i + 1}</option>
                ))
              )}
            </Field>
          </div>
          <div className="propertiesFormRow">
            Number of Baths
            <Field component="select" name="numberBaths">
              {(
                new Array(10).fill({ value: 1, label: 1 }).map((_, i) => (
                  <option key={`numberBaths-form-${i + 1}`} value={i + 1}>{i + 1}</option>
                ))
              )}
            </Field>
          </div>
          <div className="propertiesFormRow">
            Parking Spaces
            <Field component="select" name="numberParking">
              {(
                new Array(5).fill({ value: 1, label: 1 }).map((_, i) => (
                  <option key={`numberParking-form-${i + 1}`} value={i + 1}>{i + 1}</option>
                ))
              )}
            </Field>
          </div>
          <div className="propertiesFormRow">
            Number of Floors
            <Field component="select" name="numberFloors">
              {(
                new Array(5).fill({ value: 1, label: 1 }).map((_, i) => (
                  <option key={`numbesFloors-form-${i + 1}`} value={i + 1}>{i + 1}</option>
                ))
              )}
            </Field>
          </div>
          <h5>Status</h5>
          <div className="propertiesFormRow">
            <RadioButtonGroup
              id="propertyStatus"
              value={values.propertyStatus}
              error={errors.propertyStatus}
            >
              <div>
                <span>For Sale</span>
                <Field
                  component={RadioButton}
                  name="propertyStatus"
                  id="forSale"
                />
              </div>
              <div>
                <span>For Rent</span>
                <Field
                  component={RadioButton}
                  name="propertyStatus"
                  id="forRent"
                />
              </div>
            </RadioButtonGroup>
          </div>
          <h5>Location</h5>
          <Map
            googleMapURL={GOOGLE_MAP_URL}
            lat={lat || values.lat}
            lng={lng || values.lng}
            defaultZoom={8}
            onChangeMarker={onChangeMarker}
            loadingElement={<div style={{ height: '100%' }} />}
            containerElement={<div style={{ height: '400px' }} />}
            mapElement={<div style={{ height: '100%' }} />}
            isMarkerShown
          />
          <h5>Pets Allowed</h5>
          <div className="propertiesFormRow">
            <RadioButtonGroup
              id="propertyPetsAllowed"
              value={values.propertyPetsAllowed}
              error={errors.propertyPetsAllowed}
            >
              <div>
                <span>Yes</span>
                <Field
                  component={RadioButton}
                  name="propertyPetsAllowed"
                  id="petsAllowedPostive"
                />
              </div>
              <div>
                <span>No</span>
                <Field
                  component={RadioButton}
                  name="propertyPetsAllowed"
                  id="petsAllowedNegative"
                />
              </div>
            </RadioButtonGroup>
          </div>
          <div className="propertiesFormRow">
            Type of Property
            <Field component="select" name="typeProperty">
              <option value="room">room</option>
              <option value="house">house</option>
              <option value="condo">condo</option>
              <option value="apartment">apartment</option>
            </Field>
          </div>
          <h5>Has Private Security</h5>
          <div className="propertiesFormRow">
            <RadioButtonGroup
              id="hasPrivateSecurity"
              value={values.hasPrivateSecurity}
              error={errors.hasPrivateSecurity}
            >
              <div>
                <span>Yes</span>
                <Field
                  component={RadioButton}
                  name="hasPrivateSecurity"
                  id="hasPrivateSecurityPostive"
                />
              </div>
              <div>
                <span>No</span>
                <Field
                  component={RadioButton}
                  name="hasPrivateSecurity"
                  id="hasPrivateSecurityNegative"
                />
              </div>
            </RadioButtonGroup>
          </div>
          <div className="propertiesFormRow">
            Max number of occupants Allowed
            <Field component="select" name="maxOccupants">
              {(
                new Array(15).fill({ value: 1, label: 1 }).map((_, i) => (
                  <option key={`maxOccupants-form-${i + 1}`} value={i + 1}>{i + 1}</option>
                ))
              )}
            </Field>
          </div>
          <div className="propertiesFormRow">
            <RadioButtonGroup
              id="isPublished"
              value={values.isPublished}
              error={errors.isPublished}
            >
              <div>
                <span>Yes</span>
                <Field
                  component={RadioButton}
                  name="isPublished"
                  id="publishedPostive"
                />
              </div>
              <div>
                <span>No</span>
                <Field
                  component={RadioButton}
                  name="isPublished"
                  id="publishedNegative"
                />
              </div>
            </RadioButtonGroup>
          </div>
          <button className="btn btn-primary" type="submit" disabled={isSubmitting}>
            {isEditingForm ? 'Submit' : 'Add Property'}
          </button>
        </Form>
      )}
    </Formik>
  </Fragment>
);

UserSettingsForm.defaultProps = {
  lat: 0,
  lng: 0,
};

UserSettingsForm.propTypes = {
  isEditingForm: PropTypes.bool.isRequired,
  lat: PropTypes.number,
  lng: PropTypes.number,
  onChangeMarker: PropTypes.func.isRequired,
  initialValues: PropTypes.object.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onValidateErrors: PropTypes.func.isRequired,
};

export default UserSettingsForm;
