import React from 'react';
import { hot } from 'react-hot-loader/root';

import { ErrorBoundary } from 'components';
import Routes from './routes';

const Root = () => (
  <ErrorBoundary>
    <Routes />
  </ErrorBoundary>
);

export default hot(Root);
