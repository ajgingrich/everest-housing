import {
  PROPERTIES_ADD,
} from 'constants/actionTypes';

import { reducerCreator } from './reducerCreator';

const onPropertiesAddSuccess = (data) => {
  console.log(data, 'data')
};

const propertiesAdd = (state = [], action) => reducerCreator({ state, action, type: PROPERTIES_ADD, onSuccess: onPropertiesAddSuccess });

const properties = (state = [], action) => {
  if (!action || !action.type) return state;

  switch (true) {
    case action.type.indexOf(PROPERTIES_ADD) !== -1:
      return propertiesAdd(state, action);
    default:
      return state;
  }
};

export { properties };
