const isValidUser = user => user && !!user.email;

const isUserLoading = user => user && user === 'loading';

const userLoginError = user => user && user.error;

export {
  isValidUser,
  isUserLoading,
  userLoginError,
};
