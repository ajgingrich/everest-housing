import { combineReducers } from 'redux';
import { createResponsiveStateReducer } from 'redux-responsive';
import { defaultBreakpoints } from 'constants/constants';

import { keyUser, keyProperties } from 'helpers/keys';
import { user } from './users';
import { properties } from './properties';


const reducer = combineReducers({
  browser: createResponsiveStateReducer(defaultBreakpoints),
  [keyUser()]: user,
  [keyProperties()]: properties,
});

export default reducer;
