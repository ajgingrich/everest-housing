import axios from 'axios';

import { ACTION_SUCCESS, ACTION_PENDING, ACTION_FAILURE } from 'constants/actionTypes';

export const loadingAction = ({ key, type }) => ({ type: `${type}.${ACTION_PENDING}`, key });
export const actionSuccess = ({ resp, key, type }) => ({ type: `${type}.${ACTION_SUCCESS}`, payload: resp, key });
export const actionFailure = ({ error, key, type }) => ({ type: `${type}.${ACTION_FAILURE}`, payload: error, key });
export const synchronousAction = ({ type, key }) => ({ type, key });

export function peformPost({ type, location, key, body, token }) {
  const config = {
    headers: { Authorization: `Bearer ${token}` },
  };

  return function action(dispatch) {
    dispatch(loadingAction({ type, key }));

    return axios.post(location, body, token ? config : {}).then(
      resp => dispatch(actionSuccess({ resp: resp.data, key, type })),
      error => dispatch(actionFailure({ error, key, type })),
    );
  };
}

export function performPut({ type, location, key, body, token }) {
  const config = {
    headers: { Authorization: `Bearer ${token}` },
  };

  return function action(dispatch) {
    dispatch(loadingAction({ type, key }));

    return axios.put(location, body, token ? config : {}).then( // eslint-disable-line
      resp => dispatch(actionSuccess({ resp: resp.data, key, type })),
      error => dispatch(actionFailure({ error, key, type })),
    );
  };
}

export function synchronousActionCreator({ type, key }) {
  return function action(dispatch) {
    dispatch(synchronousAction({ type, key }));
  };
}
