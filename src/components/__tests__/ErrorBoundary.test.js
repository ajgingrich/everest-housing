import React from 'react'
import ErrorBoundary from 'components/ErrorBoundary.jsx'
import ShallowRenderer from 'react-test-renderer/shallow'

const renderer = new ShallowRenderer()

it('ErrorBoundary renders correctly', () => {
  const tree = renderer
    .render(
      <ErrorBoundary />
    );
  expect(tree).toMatchSnapshot();
});
