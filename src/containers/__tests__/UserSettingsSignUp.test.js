import React from 'react'
import { UserSettingsSignUp } from 'containers/UserSettings/UserSettingsSignUp'
import ShallowRenderer from 'react-test-renderer/shallow'

const renderer = new ShallowRenderer()

it('UserSettingsSignUp renders correctly', () => {
  const tree = renderer
    .render(
      <UserSettingsSignUp />
    );
  expect(tree).toMatchSnapshot();
});
