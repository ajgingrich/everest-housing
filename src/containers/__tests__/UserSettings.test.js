import React from 'react'
import { UserSettings } from 'containers/UserSettings/UserSettings'
import ShallowRenderer from 'react-test-renderer/shallow'

const renderer = new ShallowRenderer()

it('UserSettings renders correctly', () => {
  const tree = renderer
    .render(
      <UserSettings />
    );
  expect(tree).toMatchSnapshot();
});
