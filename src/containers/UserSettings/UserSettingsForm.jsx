import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import {
  Formik,
  Form,
  Field,
  ErrorMessage,
} from 'formik';

import { RadioButton, RadioButtonGroup } from 'components/Form';

const UserSettingsForm = ({ hasUser, loginError, initialValues, onValidateErrors, onSubmit }) => (
  <Fragment>
    <Formik
      initialValues={initialValues}
      validate={values => onValidateErrors(values)}
      onSubmit={(values, { setSubmitting }) => onSubmit(values, setSubmitting)}
    >
      {({ isSubmitting, values, errors }) => (
        <Form className="signUpContainer">
          {!hasUser && (
            <Fragment>
              <span>Email</span> <Field type="email" name="email" />
              <ErrorMessage className="formError" name="email" component="div" />
              {loginError &&
                <span className="loginError">{loginError}</span>
              }
              Password <Field type="password" name="password" />
              <ErrorMessage name="password" component="div" />
              Confirm Password <Field type="password" name="passwordConfirm" />
              <ErrorMessage name="passwordConfirm" component="div" />
            </Fragment>
          )}
          <RadioButtonGroup
            id="radioGroup"
            label="Role"
            value={values.radioGroup}
            error={errors.radioGroup}
          >
            <div>
              <span>User</span>
              <Field
                component={RadioButton}
                name="radioGroup"
                id="user"
              />
            </div>
            <div>
              <span>Owner</span>
              <Field
                component={RadioButton}
                name="radioGroup"
                id="owner"
              />
            </div>
          </RadioButtonGroup>
          First Name <Field type="text" name="firstName" />
          <ErrorMessage name="firstName" component="div" />
          Last Name <Field type="text" name="lastName" />
          <ErrorMessage name="lastName" component="div" />
          Twitter <Field type="text" name="twitter" />
          <ErrorMessage name="twitter" component="div" />
          <button className="btn btn-primary" type="submit" disabled={isSubmitting}>
            {hasUser ? 'Update Settings' : 'Sign Up'}
          </button>
        </Form>
      )}
    </Formik>
  </Fragment>
);

UserSettingsForm.defaultProps = {
  loginError: '',
};

UserSettingsForm.propTypes = {
  hasUser: PropTypes.bool.isRequired,
  initialValues: PropTypes.object.isRequired,
  loginError: PropTypes.string,
  onSubmit: PropTypes.func.isRequired,
  onValidateErrors: PropTypes.func.isRequired,
};

export default UserSettingsForm;
