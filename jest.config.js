module.exports = {
  collectCoverageFrom: ['src/**/*.{js,jsx}', 'server/**/*.{js,jsx}', '!src/dist/**/*.js'],
  moduleNameMapper: {
    '^helpers/(.*)$': '<rootDir>/src/helpers/$1',
    '^actions/(.*)$': '<rootDir>/src/actions/$1',
    '^constants/(.*)$': '<rootDir>/src/constants/$1',
    '^components(.*)$': '<rootDir>/src/components/$1',
    '^containers(.*)$': '<rootDir>/src/containers/$1',
    '^store': '<rootDir>/src/store/',
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$': '<rootDir>/src/img/__mocks__/imageMock.js', // eslint-disable-line max-len
  },
  testEnvironment: 'node',
  testPathIgnorePatterns: ['/node_modules/', '/test', '/build/'],
  transform: {
    '^.+\\.jsx?$': 'babel-jest',
  },
  verbose: false,
};
