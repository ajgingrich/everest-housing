/* eslint-disable no-console */
import bcrypt from 'bcrypt';

import { generateJWTToken } from '../auth/auth';

const pool = require('../db');

exports.logout = (req, res) => {
  req.session.destroy();

  res.send({});
};

exports.get = (req, res) => {
  if (req.session && req.session.user && req.session.user.email) {
    const token = generateJWTToken(req.session.user.email);
    req.session.user.token = token;
  }

  res.send(req.session.user || {});
};

exports.update = async (req, res) => {
  const body = Object.assign({}, req.body);

  try {
    const results = await pool.query(
      'CALL updateUser(?, ?, ?, ?, ?)',
      [body.id, body.role, body.firstName, body.lastName, body.twitter],
    );
    const queryResults = Object.assign({}, results[0]['0']);

    const { id, username: email, user_role: role, first_name: firstName, last_name: lastName, twitter } = queryResults || {};
    const token = generateJWTToken(email);

    req.session.user = { id, email, role, firstName, lastName, twitter, token };
    res.send({ id, email, role, firstName, lastName, twitter, token });
  } catch (error) {
    res.sendStatus(400);
    throw new Error(error);
  }
};

exports.delete = async (req, res) => {
  const body = Object.assign({}, req.body);

  try {
    const results = await pool.query( // eslint-disable-line no-unused-vars
      'CALL deleteUser(?)',
      [body.email],
    );

    req.session.user = {};
    res.send({ email: body.email });
  } catch (error) {
    res.sendStatus(400);
    throw new Error(error);
  }
};

exports.login = async (req, res) => {
  const body = Object.assign({}, req.body);
  const invalidError = 'Invalid email or password';

  try {
    const results = await pool.query('CALL selectUser(?)', [body.email]);

    if (!results.length) {
      res.send({ error: invalidError });
    }

    const queryResults = Object.assign({}, results[0][0]);
    const {
      id,
      username: email,
      password: hash,
      user_role: role,
      first_name: firstName,
      last_name: lastName,
      twitter,
    } = queryResults || {};
    const isValidPassword = await bcrypt.compare(body.password, hash);

    if (isValidPassword) {
      const token = generateJWTToken(email);
      req.session.user = { id, email, role, firstName, lastName, twitter, token };
      res.send({ id, email, role, firstName, lastName, twitter, token });
    } else {
      res.send({ error: invalidError });
    }
  } catch (error) {
    res.sendStatus(400);
    throw new Error(error);
  }
};

exports.addUser = async (req, res) => {
  const saltRounds = 10;
  const body = Object.assign({}, req.body);

  const hashedPassword = await new Promise((resolve, reject) => {
    bcrypt.hash(body.password, saltRounds, (error, hash) => {
      if (error) reject(error);
      resolve(hash);
    });
  });

  try {
    const result = await pool.query('CALL addUser(?, ?, ?, ?, ?, ?)', [
      body.email,
      hashedPassword,
      body.role,
      body.firstName,
      body.lastName,
      body.twitter,
    ]);
    const queryResults = Object.assign({}, result[0][0]);
    const { id, username: email, user_role: role, first_name: firstName, last_name: lastName, twitter } = queryResults;

    req.session.user = { id, email, role, firstName, lastName, twitter };
    res.send({ id, email, role, firstName, lastName, twitter });
  } catch (error) {
    if (error.code === 'ER_DUP_ENTRY') {
      res.send({ error: 'Email address already in use' });
    } else {
      res.sendStatus(400);
      throw new Error(error);
    }
  }
};
