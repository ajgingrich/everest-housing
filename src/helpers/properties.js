
const hasPropertiesValidation = properties => properties && !!properties.length;

const arePropertiesLoading = properties => properties && properties === 'loading';

export {
  hasPropertiesValidation,
  arePropertiesLoading,
};
