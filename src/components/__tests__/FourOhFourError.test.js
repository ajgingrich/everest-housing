import React from 'react'
import FourOhFourError from 'components/FourOhFourError.jsx'
import ShallowRenderer from 'react-test-renderer/shallow'

const renderer = new ShallowRenderer()

it('FourOhFourError renders correctly', () => {
  const tree = renderer
    .render(
      <FourOhFourError />
    );
  expect(tree).toMatchSnapshot();
});
