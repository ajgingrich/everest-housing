import { keyUser } from 'helpers/keys';
import {
  USER_FETCH,
  USER_LOGIN,
  USER_SIGN_UP,
  USER_LOGOUT,
  USER_CLEAR,
  USER_UPDATE,
} from 'constants/actionTypes';
import {
  API_USER_FETCH,
  API_USER_LOGIN,
  API_USER_LOGOUT,
  API_USER_ADD,
  API_USER_UPDATE,
} from 'constants/api';

import { peformPost, performPut, synchronousActionCreator } from './actionCreator';

export function userFetch(query) {
  return peformPost({
    type: USER_FETCH,
    key: keyUser(),
    location: API_USER_FETCH,
    body: query,
  });
}

export function userLogin(query) {
  return peformPost({
    type: USER_LOGIN,
    key: keyUser(),
    location: API_USER_LOGIN,
    body: query,
  });
}

export function userUpdate(query, token) {
  return performPut({
    type: USER_UPDATE,
    key: keyUser(),
    location: `${API_USER_UPDATE}`,
    body: query,
    token,
  });
}

export function userSignUp(query) {
  return peformPost({
    type: USER_SIGN_UP,
    key: keyUser(),
    location: API_USER_ADD,
    body: query,
  });
}

export function userLogout(query) {
  return peformPost({
    type: USER_LOGOUT,
    key: keyUser(),
    location: API_USER_LOGOUT,
    body: query,
  });
}

export function userClear() {
  return synchronousActionCreator({
    type: USER_CLEAR,
  });
}
