import React from 'react'
import { Login } from 'containers/Navigation/Login'
import ShallowRenderer from 'react-test-renderer/shallow'

const renderer = new ShallowRenderer()

it('Login renders correctly', () => {
  const tree = renderer
    .render(
      <Login />
    );
  expect(tree).toMatchSnapshot();
});
