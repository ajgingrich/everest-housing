import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { userLoginError, isValidUser, isUserLoading } from 'helpers/users';
import { userUpdate } from 'actions/users';

import UserSettingsSignUp from './UserSettingsSignUp';
import UserSettingsForm from './UserSettingsForm';

const USER_ROLE = {
  1: 'user',
  2: 'owner',
};

const mapStateToProps = ({ user }) => ({
  user,
  hasUser: isValidUser(user),
  loginError: userLoginError(user),
  loading: isUserLoading(user),
});

const mapDispatchToProps = dispatch => bindActionCreators({
  update: userUpdate,
}, dispatch);

const validateErrors = (values) => {
  // add more validation here..
  const errors = {};

  return errors;
};

export class UserSettings extends Component {
  handleUserUpdate = (values, setSubmitting) => {
    const { user, update } = this.props;
    const query = {
      ...values,
      role: values.radioGroup === 'user' ? 1 : 2,
      id: user.id,
    };

    update(query, user.token);
    setSubmitting(false);
  }

  render() {
    const { user, hasUser, loading, loginError } = this.props;

    if (loading) return <div>Loading...</div>;

    if (!hasUser) return <UserSettingsSignUp />;

    const initialValues = {
      radioGroup: USER_ROLE[user.role],
      firstName: user.firstName,
      lastName: user.lastName,
      twitter: user.twitter,
    };

    return (
      <div>
        <h2>Your information</h2>
        <UserSettingsForm
          onSubmit={this.handleUserUpdate}
          initialValues={initialValues}
          loginError={loginError}
          onValidateErrors={validateErrors}
          hasUser
        />
      </div>
    );
  }
}

UserSettings.defaultProps = {
  user: {},
  hasUser: false,
  loginError: '',
  loading: false,
};

UserSettings.propTypes = {
  user: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  hasUser: PropTypes.bool,
  loginError: PropTypes.string,
  loading: PropTypes.bool,
  update: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(UserSettings);
