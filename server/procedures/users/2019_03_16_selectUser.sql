CREATE PROCEDURE everest.`selectUser`
(p_username varchar(32))
BEGIN
	SELECT id, username, password, user_role, first_name, last_name, twitter FROM everest.user
	WHERE username = p_username;
END;
