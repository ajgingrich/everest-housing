// users
export const USER_FETCH = 'USER_FETCH';
export const USER_SIGN_UP = 'USER_SIGN_UP';
export const USER_LOGIN = 'USER_LOGIN';
export const USER_LOGOUT = 'USER_LOGOUT';
export const USER_CLEAR = 'USER_CLEAR';
export const USER_UPDATE = 'USER_UPDATE';

// properties
export const PROPERTIES_ADD = 'PROPERTIES_ADD';

// action and reducer bindAction
export const ACTION_PENDING = 'PENDING';
export const ACTION_SUCCESS = 'SUCCESS';
export const ACTION_FAILURE = 'FAILURE';
