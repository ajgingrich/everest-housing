import {
  USER_FETCH,
  USER_LOGIN,
  USER_SIGN_UP,
  USER_LOGOUT,
  USER_CLEAR,
  USER_UPDATE,
} from 'constants/actionTypes';

import { reducerCreator } from './reducerCreator';

const onUserSuccess = data => data;

const onUserLogoutSuccess = () => ({});

const userFetch = (state = {}, action) => reducerCreator({ state, action, type: USER_FETCH, onSuccess: onUserSuccess });
const userLogin = (state = {}, action) => reducerCreator({ state, action, type: USER_LOGIN, onSuccess: onUserSuccess });
const userSignUp = (state = {}, action) => reducerCreator({ state, action, type: USER_SIGN_UP, onSuccess: onUserSuccess });
const userUpdate = (state = {}, action) => reducerCreator({ state, action, type: USER_UPDATE, onSuccess: onUserSuccess });
const userLogout = (state = {}, action) => reducerCreator({ state, action, type: USER_LOGOUT, onSuccess: onUserLogoutSuccess });

const user = (state = {}, action) => {
  if (!action || !action.type) return state;

  switch (true) {
    case action.type.indexOf(USER_FETCH) !== -1:
      return userFetch(state, action);
    case action.type.indexOf(USER_LOGIN) !== -1:
      return userLogin(state, action);
    case action.type.indexOf(USER_SIGN_UP) !== -1:
      return userSignUp(state, action);
    case action.type.indexOf(USER_UPDATE) !== -1:
      return userUpdate(state, action);
    case action.type.indexOf(USER_LOGOUT) !== -1:
      return userLogout(state, action);
    case action.type.indexOf(USER_CLEAR) !== -1:
      return {};
    default:
      return state;
  }
};

export { user };
