import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router';
import { Formik, Form, Field, ErrorMessage } from 'formik';

const SETTINGS_PATH_NAME = '/user/settings';

const validateErrors = (values) => {
  const errors = {};

  if (!values.email) {
    errors.email = 'Required';
  } else if (
    !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
  ) {
    errors.email = 'Invalid email address';
  }

  return errors;
};

export class Login extends Component {
  componentWillUnmount() {
    const { user, onUserClear } = this.props;

    if (user && user.error) {
      onUserClear();
    }
  }

  handleSubmit = (values, setSubmitting) => {
    const { onLogin } = this.props;

    onLogin(values);
    setSubmitting(false);
  }

  handleGoToSettings = () => {
    const { history, onCloseLogin } = this.props;

    if (history.location.pathname !== SETTINGS_PATH_NAME) {
      history.push(SETTINGS_PATH_NAME);
    }

    onCloseLogin();
  }

  render() {
    const { loginError } = this.props;

    return (
      <div>
        <Formik
          initialValues={{ email: '', password: '' }}
          validate={values => validateErrors(values)}
          onSubmit={(values, { setSubmitting }) => this.handleSubmit(values, setSubmitting)}
        >
          {({ isSubmitting }) => (
            <Form className="loginForm">
              email <Field type="email" name="email" />
              <ErrorMessage className="formError" name="email" component="div" />
              password <Field type="password" name="password" />
              <ErrorMessage name="password" component="div" />
              <button className="btn btn-primary" type="submit" disabled={isSubmitting}>
               Submit
              </button>
            </Form>
          )}
        </Formik>
        {loginError &&
          <span className="loginError">{loginError}</span>
        }
        <span className="registerLink">
          Not Registered? Sign up <span className="settingsLink" onClick={this.handleGoToSettings}>here</span>
        </span>
      </div>
    );
  }
}

Login.defaultProps = {
  loginError: '',
  user: {},
};

Login.propTypes = {
  user: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  history: PropTypes.object.isRequired,
  loginError: PropTypes.string,
  onCloseLogin: PropTypes.func.isRequired,
  onUserClear: PropTypes.func.isRequired,
  onLogin: PropTypes.func.isRequired,
};

export default withRouter(Login);
