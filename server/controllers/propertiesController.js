/* eslint-disable no-console */
const pool = require('../db');

exports.add = async (req, res) => {
  const body = Object.assign({}, req.body);

  try {
    const results = await pool.query(
      'CALL addProperty(?, ?, ?, ?, ?)', [
        body.userId,
        body.rentPerMonth,
        body.physicalDescription,
        body.numberRooms,
        body.numberBaths,
        body.numberParking,
        body.numberFloors,
        body.forSale ? 1 : 0,
        body.lat,
        body.lng,
        body.mascots_allowed ? 1 : 0,
        body.typeProperty,
        body.hasPrivateSecurity ? 1 : 0,
        body.maxOccupants,
      ],
    );
    // const queryResults = Object.assign({}, results[0]['0']);

    // const {
    //   id,
    //   username: email,
    //   user_role: role,
    //   first_name: firstName,
    //   last_name: lastName,
    //   twitter,
    // } = queryResults || {};

    res.sendStatus(200);
  } catch (error) {
    res.sendStatus(400);
    throw new Error(error);
  }
};
