import path from 'path';
import express from 'express';

import {
  API_USER_LOGIN,
  API_USER_FETCH,
  API_USER_ADD,
  API_USER_LOGOUT,
  API_USER_UPDATE,
  API_USER_DELETE,
  API_PROPERTIES_ADD,
} from '../../src/constants/api';
import userController from '../controllers/userController';
import propertiesController from '../controllers/propertiesController';

const router = express.Router();

// USERS
router.post(API_USER_FETCH, userController.get);
router.put(API_USER_UPDATE, userController.update);
router.post(API_USER_LOGIN, userController.login);
router.post(API_USER_ADD, userController.addUser);
router.post(API_USER_LOGOUT, userController.logout);
router.delete(API_USER_DELETE, userController.delete);

// PROPERTIES
router.post(API_PROPERTIES_ADD, propertiesController.add)

// home page
router.get('*', (req, res) => {
  res.sendFile(path.join(__dirname), '../../index.html');
});

module.exports = router;
