CREATE PROCEDURE everest.`addProperty`
(
	p_user_id INT,
	p_rent_per_month INT,
	p_physical_description VARCHAR(1000),
	p_number_rooms INT,
	p_number_baths INT,
	p_number_parking INT,
	p_number_floors INT,
	p_for_sale TINYINT,
	p_lat DECIMAL(10, 8),
	p_lng DECIMAL(11, 8),
	p_mascots_allowed TINYINT,
	p_type_property VARCHAR(32),
	p_has_private_security TINYINT,
	p_max_occupants INT
)
BEGIN
	INSERT INTO everest.properties
	(
		user_id,
		rent_per_month,
		physical_description,
		number_rooms,
		number_baths,
		number_parking,
		number_floors,
		for_sale,
		lat,
		lng,
		mascots_allowed,
		type_property,
		has_private_security,
		max_occupants
	)
	VALUES(
		p_user_id,
		p_rent_per_month,
		p_physical_description,
		p_number_rooms,
		p_number_baths,
		p_number_parking,
		p_number_floors,
		p_for_sale,
		p_lat,
		p_lng,
		p_mascots_allowed,
		p_type_property,
		p_has_private_security,
		p_max_occupants
	);
END;
