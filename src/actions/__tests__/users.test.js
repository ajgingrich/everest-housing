import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import moxios from 'moxios'
import axios from 'axios'

import * as types from 'constants/actionTypes'
import { keyUser } from 'helpers/keys';
import { ACTION_SUCCESS, ACTION_PENDING, ACTION_FAILURE } from 'constants/actionTypes';
import { LOADING } from 'constants/constants';
import { userSignUp, userLogin, userFetch, userUpdate, userLogout } from 'actions/users'

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

describe('Users Actions', () => {
  beforeEach(() => {
    moxios.install(axios)
  })

  afterEach(() => {
    moxios.uninstall(axios)
  })

  const mockUserQuery = {
    email: 'admin-mock@yahoo.com',
    password: 'test123',
    role: 1,
    firstName: 'john',
    lastName: 'smith',
    twitter: 'tweety',
  };

  const mockUserResponse = {
    email: 'admin-mock@yahoo.com',
    role: 1,
    firstName: 'john',
    lastName: 'smith',
    twitter: 'tweety',
  };

  it('creates USER_SIGN_UP.SUCCESS', () => {
    moxios.wait(() => {
      const request = moxios.requests.mostRecent();
      request.respondWith({
        status: 200,
        response: mockUserResponse,
      });
    });

    const expectedActions = [
      {
        type: `${types.USER_SIGN_UP}.${ACTION_PENDING}`,
        key: keyUser(),
      },
      {
        type: `${types.USER_SIGN_UP}.${ACTION_SUCCESS}`,
        key: keyUser(),
        payload: mockUserResponse,
      }
    ]
    const store = mockStore({ [keyUser()]: {} })

    return (
      store
      .dispatch(userSignUp())
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions)
      })
    )
  })

  it('creates API_USER_LOGIN.SUCCESS', () => {
    moxios.wait(() => {
      const request = moxios.requests.mostRecent();
      request.respondWith({
        status: 200,
        response: mockUserResponse,
      });
    });

    const expectedActions = [
      {
        type: `${types.USER_LOGIN}.${ACTION_PENDING}`,
        key: keyUser(),
      },
      {
        type: `${types.USER_LOGIN}.${ACTION_SUCCESS}`,
        key: keyUser(),
        payload: mockUserResponse,
      }
    ]
    const store = mockStore({ [keyUser()]: {} })

    return (
      store
      .dispatch(userLogin())
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions)
      })
    )
  })

  it('creates API_USER_FETCH.SUCCESS', () => {
    moxios.wait(() => {
      const request = moxios.requests.mostRecent();
      request.respondWith({
        status: 200,
        response: mockUserResponse,
      });
    });

    const expectedActions = [
      {
        type: `${types.USER_FETCH}.${ACTION_PENDING}`,
        key: keyUser(),
      },
      {
        type: `${types.USER_FETCH}.${ACTION_SUCCESS}`,
        key: keyUser(),
        payload: mockUserResponse,
      }
    ]
    const store = mockStore({ [keyUser()]: {} })

    return (
      store
      .dispatch(userFetch())
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions)
      })
    )
  })

  it('creates API_USER_UPDATE.SUCCESS', () => {
    moxios.wait(() => {
      const request = moxios.requests.mostRecent();
      request.respondWith({
        status: 200,
        response: mockUserResponse,
      });
    });

    const expectedActions = [
      {
        type: `${types.USER_UPDATE}.${ACTION_PENDING}`,
        key: keyUser(),
      },
      {
        type: `${types.USER_UPDATE}.${ACTION_SUCCESS}`,
        key: keyUser(),
        payload: mockUserResponse,
      }
    ]
    const store = mockStore({ [keyUser()]: {} })

    return (
      store
      .dispatch(userUpdate())
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions)
      })
    )
  })

  it('creates API_USER_LOGOUT.SUCCESS', () => {
    moxios.wait(() => {
      const request = moxios.requests.mostRecent();
      request.respondWith({
        status: 200,
        response: mockUserResponse,
      });
    });

    const expectedActions = [
      {
        type: `${types.USER_LOGOUT}.${ACTION_PENDING}`,
        key: keyUser(),
      },
      {
        type: `${types.USER_LOGOUT}.${ACTION_SUCCESS}`,
        key: keyUser(),
        payload: mockUserResponse,
      }
    ]
    const store = mockStore({ [keyUser()]: {} })

    return (
      store
      .dispatch(userLogout())
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions)
      })
    )
  })
})
