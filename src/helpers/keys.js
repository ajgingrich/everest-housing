const keyUser = () => 'user';
const keyProperties = () => 'properties';
const keyProperty = id => `property-${id}`;

export { keyUser, keyProperty, keyProperties };
