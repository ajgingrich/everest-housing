import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { hasPropertiesValidation, arePropertiesLoading } from 'helpers/properties';
import { propertyAdd } from 'actions/properties';

import PropertiesForm from './PropertiesForm';

const INITIAL_VALUES = {
  rentPerMonth: '',
  physicalDescription: '',
  numberRooms: 1,
  numberBaths: 1,
  numberParking: 1,
  numberFloors: 1,
  propertyStatus: 'forSale',
  lat: 37.226361,
  lng: -80.420739,
  propertyPetsAllowed: 'petsAllowedPostive',
  typeProperty: 'apartment',
  hasPrivateSecurity: 'hasPrivateSecurityNegative',
  maxOccupants: 2,
  isPublished: 'publishedNegative',
};

const mapStateToProps = ({ properties, user }) => ({
  user,
  properties,
  hasProperties: hasPropertiesValidation(properties),
  loading: arePropertiesLoading(properties),
});

const mapDispatchToProps = dispatch => bindActionCreators({
  addProperty: propertyAdd,
}, dispatch);

class Properties extends Component {
  constructor(props) {
    super(props);

    this.state = { isEditing: false, lat: 0, lng: 0 };
  }

  handleAddProperty = (values, setSubmitting) => {
    const { user, addProperty } = this.props;

    console.log(values, 'values')

    // const query = {
    //   ...values,
    //   role: values.radioGroup === 'user' ? 1 : 2,
    //   id: user.id,
    // };
    //
    // addProperty(query, user.token);
    // setSubmitting(false);
  }

  handleChangeLocation = ({ latLng }) => {
    const { lat: mapLat, lng: mapLng } = latLng || {};
    const lat = mapLat().toPrecision(8);
    const lng = mapLng().toPrecision(8);

    this.setState({ lat: parseFloat(lat), lng: parseFloat(lng) });
  }

  render() {
    const { properties, hasProperties, loading } = this.props;
    const { isEditing, lat, lng } = this.state;

    if (loading) return <div>Loading...</div>;

    return (
      <div className="properties">
        {hasProperties && <button className="btn btn-primary">Edit</button>}
        {isEditing && <div>editing</div>}
        {!isEditing && (
          <Fragment>
            <h3>Add Property</h3>
            <PropertiesForm
              initialValues={INITIAL_VALUES}
              lat={lat}
              lng={lng}
              onChangeMarker={this.handleChangeLocation}
              onSubmit={this.handleAddProperty}
              onValidateErrors={() => console.log('errors')}
              isEditingForm={false}
            />
          </Fragment>
        )}
      </div>
    );
  }
}

Properties.defaultProps = {
  user: {},
  properties: [],
  hasProperties: false,
  loading: false,
};

Properties.propTypes = {
  properties: PropTypes.oneOfType([PropTypes.array, PropTypes.string]),
  user: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  addProperty: PropTypes.func.isRequired,
  hasProperties: PropTypes.bool,
  loading: PropTypes.bool,
};

export default connect(mapStateToProps, mapDispatchToProps)(Properties);
