import https from 'https';
import fs from 'fs';
import path from 'path';

import app from './app';

const port = 4040;

// import http from 'http';
// TODO: look at this when deploying to AWS
// if (app.get('env') === 'development') { }
// const server = http.createServer(app);

const server = https.createServer({
  key: fs.readFileSync(path.resolve('./server/server.key')),
  cert: fs.readFileSync(path.resolve('./server/server.crt')),
}, app);


server.listen(port, () => {
  console.log(`Node.js listening on port ${port}...`); // eslint-disable-line no-console
});

export default server;
