const BASE_URL = '/api';

// users
const API_USER_FETCH = `${BASE_URL}/user/fetch`;
const API_USER_ADD = `${BASE_URL}/user/add`;
const API_USER_LOGIN = `${BASE_URL}/user/login`;
const API_USER_LOGOUT = `${BASE_URL}/user/logout`;
const API_USER_UPDATE = `${BASE_URL}/user/update`;
const API_USER_DELETE = `${BASE_URL}/user/delete`;

// properties
const API_PROPERTIES_ADD = `${BASE_URL}/properties/add`;

module.exports = {
  API_USER_FETCH,
  API_USER_ADD,
  API_USER_LOGIN,
  API_USER_LOGOUT,
  API_USER_UPDATE,
  API_USER_DELETE,
  API_PROPERTIES_ADD,
};
