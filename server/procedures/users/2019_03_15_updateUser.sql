CREATE PROCEDURE everest.`updateUser`
(p_id int, p_userrole int, p_firstname varchar(32), p_lastname varchar(32), p_twitter varchar(32))
BEGIN
    UPDATE user
    SET
    	user_role=p_userrole, first_name=p_firstname, last_name=p_lastname, twitter=p_twitter
   	WHERE id=p_id;
    SELECT * FROM everest.user WHERE id = p_id;
END;
